const fastify = require('fastify')()

/**
 * http://{domain}}:3000/documentation for swagger UI
 * http://{domain}}:3000/documentation/json for swagger json
 * http://{domain}}:3000/documentation/yaml for swagger yaml
 */

fastify.register(require('fastify-swagger'), {
  swagger: {
    info: {
      title: 'Test swagger',
      description: 'testing the fastify swagger api',
      version: '0.1.0'
    },
    host: 'localhost',
    schemes: ['http'],
    consumes: ['application/json'],
    produces: ['application/json'],
    securityDefinitions: {
      apiKey: {
        type: 'apiKey',
        name: 'apiKey',
        in: 'header'
      }
    }
  },
  exposeRoute: true
})


fastify.ready(err => {
  if (err) throw err
  fastify.swagger()
})

fastify.get('/', async (request, reply) => {
  reply.type('application/json').code(200)
  return { hello: 'world' }
})

fastify.get('/init', async (request, reply) => {
  reply.type('application/json').code(200)
  return { "route" : "in init" }
})

// generate swagger doc (json)
fastify.get('/doc', async(request, reply) => {
  // can add yaml = true to force generate yaml
  // more reference: https://github.com/fastify/fastify-swagger
  return fastify.swagger()
})

fastify.get('/some-route/:id', {
  schema: {
    description: 'post some data',
    tags: ['user', 'code'],
    summary: 'qwerty',
    params: {
      type: 'object',
      properties: {
        id: {
          type: 'string',
          description: 'user id'
        }
      }
    },
    body: {
      type: 'string',
      properties: {
        hello: { type: 'string' },
        obj: {
          type: 'object',
          properties: {
            some: { type: 'string' }
          }
        }
      }
    },
    response: {
      201: {
        description: 'Successful response',
        type: 'object',
        properties: {
          hello: { type: 'string' }
        }
      }
    },
    security: [
      {
        "api_key": []
      }
    ]
  }
}, async (req, reply) => {
  reply.type('application/json').code(200)
  return { "route" : "should be in swagger" }
})

fastify.listen(3000, 'localhost', function (err) {
  if (err) throw err
  console.log(`server listening on ${fastify.server.address().port}`)
})
