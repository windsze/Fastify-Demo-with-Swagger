# get started

1. install dependency

```sh
$ npm install
```

2. start project

```sh
$ node start
or
$ nodemon start
```

# documentation
-   http://{domain}}:3000/documentation for swagger UI
-   http://{domain}}:3000/documentation/json for swagger json
-   http://{domain}}:3000/documentation/yaml for swagger yaml
